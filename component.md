# レイヤーと構成要素

標準のレイヤー定義と構成要素を以下に示します。

![](images/layers-and-components.png)

## ドメイン

ドメインオブジェクトは以下の点に留意した、業務で扱う型の定義と型変換のことを指します。

- [命名](https://scrapbox.io/kawasima/%E5%91%BD%E5%90%8D%E3%81%AE%E3%83%97%E3%83%AD%E3%82%BB%E3%82%B9)
- [Always-Valid](https://scrapbox.io/kawasima/Always-Valid_Domain_Model)
- [異なるふるまい・不変条件には異なる型を](https://scrapbox.io/kawasima/%E3%83%89%E3%83%A1%E3%82%A4%E3%83%B3%E3%83%A2%E3%83%87%E3%83%AB%E8%B2%A7%E8%A1%80%E7%97%87)

```java
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@EqualsAndHash
public class AnswerRepository {
    private static StringValidator<String> urlValidator = StringValidatorBuilder.of("url", c -> c.url())
            .build();
    private static StringValidator<String> commitHashValidator = StringValidatorBuilder.of("commitHash", c ->
            c.lessThanOrEqual(40).greaterThanOrEqual(40))
            .build();

    public static Arguments2Validator<String, String, AnswerRepository> validator = ArgumentsValidators
            .split(urlValidator, commitHashValidator)
            .apply(AnswerRepository::new);

    @Getter
    private String url;
    @Getter
    private String commitHash;

    public static AnswerRepository of(String url, String commitHash) {
        return validator.validated(url, commitHash);
    }
}
```

## ユースケース

ユースケースには以下の2パターンが存在します。

* コマンドを受け取り、イベントを返す
* クエリを受け取り、リードモデルを返す

ユースケースシナリオの途中で、発生する代替シナリオは例外として設計します。

```java
public interface PostCommentUseCase {
    PostedCommentEvent handle(PostCommentCommand command)
        throws AnswerNotFoundException, CommenterNotFoundException, CommentsLimitedExceedException;
}
```

ユースケースをまたがないコマンドやクエリは、UseCaseインタフェースのインナークラスとして定義すると、application層のファイル数を抑えることができます。

## ユースケース実装

- コマンド/クエリからドメインオブジェクトを構築する。
- ポートを介して、データベースなど外界と通信する。
- 予期する例外はExceptionをthrowする。
- トランザクションを制御する。
- イベント/リードモデルを組み立てて返す。

## Port

以下のようなものとのやりとりが代表的なポートです。

* RDBMS
* KVS
* OS (乱数や現在時刻)
* Web API

## Adapter

### PersistenceAdapter

データを取得したり、保存したりする永続化層とのやりとりするポートの実装です。
実際のORマッパーライブラリの処理はRepositoryを提供しているものがほとんどなので、それを利用します。

### Mapper

ORマッパーのレコードオブジェクト(Entity)と、ドメインモデルの相互変換を行う。
1つのテーブルの操作が複数のPersistenceAdapterにまたがることもあるので、独立したクラスとして作ります。

### Repository

ORマッパーの提供するData Access Objectです。


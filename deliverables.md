# 成果物定義

### 開発イテレーション

* [擬人化ユースケース図](https://www.evernote.com/shard/s7/sh/d555c213-0e6d-ff98-ef7d-aa66a691ce8e/99d827857eefdfb42f93afbc1811262a)
* [ユースケースシナリオ](usecase.md)
* 型定義(コードでも可)
* 画面項目定義
* [ADR](adr.md) (必要に応じて)

ドキュメントとコードの対応
![](images/code-mapping.png)

### テストイテレーション

* [テスト計画書](https://fintan.jp/page/1456/)

